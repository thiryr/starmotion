using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship
{
    private GameObject leftCanon;
    private GameObject rightCanon;

    private GameObject ShipObject;

    private float forwardSpeed = 0.0f;
    private float forwardAcceleration = 0;
    private float yawSpeed = 0;
    private float rollSpeed = 0;
    private float pitchSpeed = 0;

    private const float maxForwardSpeed = 10.0f;

    private const float ForwardAccelerationModifier = 0.05f;
    private const float YawSpeedModifier = 30.0f;
    private const float RollSpeedModifier = 50.0f;
    private const float PitchSpeedModifier = 50.0f;


    private bool isLightSpeed = false;
    private bool isCanonLocked = true;

    public Ship(GameObject shipObject, GameObject leftCanon, GameObject rightCanon)
    {
        this.ShipObject = shipObject;
        this.leftCanon = leftCanon;
        this.rightCanon = rightCanon;
    }

    public void updateForwardAcceleration(float forwardInputValue, float deltaTime){
        this.forwardAcceleration = forwardInputValue * ForwardAccelerationModifier * deltaTime;
    }

    public void cancelMomentum(){
        this.forwardSpeed = 0;
        this.forwardAcceleration = 0;
    }

    public void updateYawSpeed(float yawInputVal, float deltaTime){
        this.yawSpeed = yawInputVal * YawSpeedModifier * deltaTime;
    }

    public void updateRollSpeed(float rollInputVal, float deltaTime){
        this.rollSpeed = rollInputVal * deltaTime * RollSpeedModifier;
    }

    public void updatePitchSpeed(float pitchInputVal, float deltaTime){
        this.pitchSpeed = pitchInputVal * deltaTime * PitchSpeedModifier;
    }


    public void updatePosition(){

        if(this.isLightSpeed){
            this.yawSpeed = 0;
            this.rollSpeed = 0;
            this.pitchSpeed = 0;

            forwardSpeed = 100;
        }else{


            float newForwardSpeed = forwardSpeed + forwardAcceleration;
            if(newForwardSpeed  > maxForwardSpeed){
                newForwardSpeed = maxForwardSpeed;
            }else if(forwardSpeed < -maxForwardSpeed){
                newForwardSpeed = -maxForwardSpeed;
            }
        forwardSpeed = newForwardSpeed;

        }


        ShipObject.transform.Translate(forwardSpeed * Vector3.forward * Time.deltaTime,Space.Self);
        ShipObject.transform.Rotate(new Vector3(pitchSpeed, yawSpeed, rollSpeed),Space.Self);


        this.yawSpeed = 0;
        this.rollSpeed = 0;
        this.pitchSpeed = 0;
    }



    public void setLightSpeed(bool on){
        this.isLightSpeed = on;
    }



    public float getForwardSpeed(){
        return this.forwardSpeed;
    }

    public float getYawSpeed(){
        return this.yawSpeed;
    }

    public float getPitchSpeed(){
        return this.pitchSpeed;
    }

    public float getRollSpeed(){
        return this.rollSpeed;
    }


    public void setCanonLock(bool locked){
        this.isCanonLocked = locked;
    }

    public void rotateCanonUpDown(float targetAngle){
        leftCanon.GetComponent<Canon>().setUpDownRotation(targetAngle);
        rightCanon.GetComponent<Canon>().setUpDownRotation(targetAngle);
    }
    public void rotateCanonLeftRight(float targetAngle){
        leftCanon.GetComponent<Canon>().setLeftRightRotation(targetAngle);
        rightCanon.GetComponent<Canon>().setLeftRightRotation(targetAngle);
    }

    public void startShooting(){
        if(!this.isCanonLocked){
            leftCanon.GetComponent<Canon>().startShooting();
            rightCanon.GetComponent<Canon>().startShooting();
        }
    }
    public void stopShooting(){
        leftCanon.GetComponent<Canon>().stopShooting();
        rightCanon.GetComponent<Canon>().stopShooting();
    }








}
