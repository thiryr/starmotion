using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

using static Ship;


public class HandControls : MonoBehaviour
{

    Ship controlledShip;
    Leap.Controller controller = null;

    //in mm from physical position of LeapMotion camera x,y,z
    Vector3 LeapOffsetFromCenter = new Vector3(0.0f,100.0f,0.0f);

    //treshold for fingers to be considered flat in radians
    float FlatFingerAngle = (float) System.Math.PI/6;
    //NOTE should probably be reduced
    //treshold for fingers to be considered next to one another (in mm)
    float TouchingDistance = 20.0f;

    const float rollDeadZone = 0.2f;
    const float pitchDeadZone = 0.2f;
    const float yawDeadZone = 0.2f;
    const float forwardDeadZone = 0.01f;


    //currently unsused, offset by speed multipliers
    //values to be remapped to [-1,1]
    //CONST
    (float,float) rollRemap = (-2,2);
    (float, float) pitchRemap = (-1, 1);
    (float, float) yawRemap = (-1, 1);
    (float,float) forwardRemap = (-1, 1);




    float debugPitch = 0;
    float debugYaw = 0;
    float debugRoll = 0;
    float debuglhRoll = 0;


    //0 not registering, 1 crossed z plane backwards,
    //2 crossed z plane forward, 3 same as 1, 4 same as 2 (last state)
    int spearActionState = 0;
    float spearActionBackwardsZCoordinate = -0.2f;
    float spearActionForwardsZCoordinate = 0.2f;
    const float spearActionBetweenStateTiming = 4; // in s (float)
    float spearActionStateStartTime = -(spearActionBetweenStateTiming);


    const float canonUnlockRoll =  -2.0f* (float) System.Math.PI/3.0f;
    const float canonLockRoll = -canonUnlockRoll;

    void Start()
    {
        //gameObject from UnityEngine
        this.controlledShip = new Ship(gameObject, gameObject.transform.GetChild(1).GetChild(1).gameObject, gameObject.transform.GetChild(1).GetChild(0).gameObject);

        controller = new Leap.Controller();
    }

    void Update () {
        if(controller != null && controller.IsConnected && controller.Devices.Count == 1){

            Hand lHand = null;
            Hand rHand = null;
            Frame frame = controller.Frame();

            if(frame.Hands.Count == 1){
                rHand = frame.Hands[0];
            }else if (frame.Hands.Count == 2){
                if(frame.Hands[0].IsRight){
                    rHand = frame.Hands[0];
                    lHand = frame.Hands[1];
                }else{
                    rHand = frame.Hands[1];
                    lHand = frame.Hands[0];
                }
            }

            handleRightHand(rHand);
            handleLeftHand(lHand);
        }

        //update the ship model and data at each update
        this.controlledShip.updatePosition();
    }

    void OnGUI(){
        GUI.Label(new Rect(10,10,200,50), "Forward Speed: " + this.controlledShip.getForwardSpeed());
        GUI.Label(new Rect(10,40,200,50), "Yaw Speed: " + this.controlledShip.getYawSpeed() + " / " + debugYaw);
        GUI.Label(new Rect(10,70,200,50), "Roll Speed: " + this.controlledShip.getRollSpeed() + " / " + debugRoll);
        GUI.Label(new Rect(10, 130, 200, 50), "state " + this.spearActionState + " " + this.spearActionStateStartTime + " " + debuglhRoll + " " + Time.time);
        GUI.Label(new Rect(10,100,200,50), "Pitch Speed: " + this.controlledShip.getPitchSpeed() + " / " + debugPitch);
    }

    private void handleRightHand(Hand hand){

        if (hand == null)
        {
            return;
        }


        handleRHAcceleration(hand);
        handleRHYaw(hand);
        handleRHPitch(hand);
        handleRHRoll(hand);
        handleRHLightspeed(hand);


    }
    private void handleLeftHand(Hand hand){

        if (hand == null)
        {
            return;
        }
        handleLHFire(hand);
    }

    private void handleLHFire(Hand hand){
        // if thumb closed enable the shoot feature else stop the shoot
        // kind of glutton way but if it works
        if(isClosedThumb(hand)){
            this.controlledShip.startShooting();
        }else{
        this.controlledShip.stopShooting();
        }


        //lock guns if turning hand
        float handRoll = hand.PalmNormal.Roll;
        debuglhRoll = handRoll;
        if (handRoll < canonLockRoll){
            this.controlledShip.setCanonLock(true);
        }else if(handRoll > canonUnlockRoll){
            this.controlledShip.setCanonLock(false);
        }

    }

    private void handleRHLightspeed(Hand hand){
        if(spearActionState == 4){
            this.controlledShip.setLightSpeed(true);
        }
        if(spearActionState > 0 && Time.time - spearActionStateStartTime > spearActionBetweenStateTiming){
            spearActionState = 0;
        }

        if(isFlatHand(hand)){
            float palmPosZ = hand.PalmPosition.z;

            if(spearActionState%2==0 && palmPosZ < spearActionBackwardsZCoordinate){
                spearActionState += 1;
                spearActionStateStartTime = Time.time;
            }else if(spearActionState%2==1 && palmPosZ > spearActionForwardsZCoordinate){
                spearActionState += 1;
                spearActionStateStartTime = Time.time;
            }

    }
    }


    private void handleRHAcceleration(Hand hand){
        if(!isFlatHand(hand)){
            //stop momentum if fully-closed hand
            if(isClosedHand(hand)){
                this.controlledShip.cancelMomentum();
                this.controlledShip.setLightSpeed(false);
            }
            updateForwardAcceleration(0);
            return;
        }

        Leap.Vector palmPos = hand.PalmPosition;

        updateForwardAcceleration(-palmPos.z);
    }


    //hand detection utility

    private bool isFlatHand(Hand hand){
        List<Finger> fingers = hand.Fingers;

        foreach(Finger finger in fingers){
            //omitting thumb
            //if angle between proximal and intermediate finger is more than FlatFingerAngle, return false
            if(finger.Type != Finger.FingerType.TYPE_THUMB && finger.Bone(Bone.BoneType.TYPE_PROXIMAL).Direction.AngleTo(finger.Bone(Bone.BoneType.TYPE_INTERMEDIATE).Direction) > FlatFingerAngle){
                return false;
            }
        }
        return true;
    }

    private bool isClosedHand(Hand hand){
        List<Finger> fingers = hand.Fingers;




        //middle finger needs to be detected
        if(fingers.Count<3)
            return false;

        //main condition
        //tip of finger needs to be closer to center of palm than 'start of finger'
        if(fingers[3].Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(fingers[3].Bone(Bone.BoneType.TYPE_PROXIMAL).PrevJoint) <  fingers[3].Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(hand.PalmPosition)){
            return false;
        }


        //check that fingers are next to one another, could be optional
        Finger prevFinger = null;
        foreach(Finger finger in fingers){
            if(finger.Type == Finger.FingerType.TYPE_THUMB){
                continue;
            }

            if(prevFinger==null){
                prevFinger=finger;
                continue;
            }
            else if(prevFinger.Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(finger.Bone(Bone.BoneType.TYPE_DISTAL).Center) > TouchingDistance){
                return false;
            }
            prevFinger=finger;
        }

        return true;



        /*foreach(Finger finger in fingers){
            //omitting thumb
            //if distal under palm, ok

            //check if distance to center of palm is less than distance to end of the metacarpal bone
            if(finger.Type != Finger.FingerType.TYPE_THUMB && finger.Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(finger.Bone(Bone.BoneType.TYPE_PROXIMAL).PrevJoint) >  finger.Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(hand.PalmPosition)){
                return false;
            }
            //NOTE if doesn't work try either set minimum distance between center and distal \
            //or try hand.SphereRadius/SphereCenter \
            //or check that the distal is "before" the end of the metacarpal\
            //(form a plane from metacarpal end position and hand.palmNormal x[Cross()] hand.Direction, check the distance between formed plane and center of distal finger)
        }*/
    }

    //only true is not closed hand too
    private bool isClosedPhalanges(Hand hand){
        List<Finger> fingers = hand.Fingers;

        //if proximal angle close to normal of the palm
        //and intermediate close to normal PI/2 from proximal Direction

        //AND/OR distal distal close to center of palm

        return false;
    }

    //detect if thumb is close -> defined by the thumb being close to the palm
    private bool isClosedThumb(Hand hand){
        List<Finger> fingers = hand.Fingers;

        // check if distal of thumb is closer to the proximal of finger proximal 1 or 2
        // if it's close to 2 then it's open else closed
        if(fingers[0].Type == Finger.FingerType.TYPE_THUMB &&
            fingers[0].Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(fingers[1].Bone(Bone.BoneType.TYPE_PROXIMAL).PrevJoint) <  fingers[0].Bone(Bone.BoneType.TYPE_DISTAL).Center.DistanceTo(fingers[2].Bone(Bone.BoneType.TYPE_PROXIMAL).PrevJoint)){
            return false;
        }
        return true;
    }




    private void handleRHYaw(Hand hand){
        float yaw = hand.Direction.Yaw;
        debugYaw = yaw;
        updateYawSpeed(yaw);
    }

    private void handleRHPitch(Hand hand){
        float pitch = hand.Direction.Pitch;
        debugPitch = pitch;
        updatePitchSpeed(-pitch);
    }

    private void handleRHRoll(Hand hand){
        float roll = hand.PalmNormal.Roll;
        debugRoll = roll;
        if (roll < rollDeadZone && roll > -rollDeadZone) {
            roll = 0;
        }
        updateRollSpeed(roll);
    }


    //ship control utility
    private void updateForwardAcceleration(float palmX){
        this.controlledShip.updateForwardAcceleration(palmX, Time.deltaTime);
    }

    private void updatePitchSpeed(float pitch){
        this.controlledShip.updatePitchSpeed(pitch, Time.deltaTime);
    }

    private void updateRollSpeed(float roll){
        this.controlledShip.updateRollSpeed(roll, Time.deltaTime);
    }
    private void updateYawSpeed(float yaw){
        this.controlledShip.updateYawSpeed(yaw, Time.deltaTime);
    }



    //unused utilities
    private static float remap(float input_a, float input_b, float output_a, float output_b, float val) {
        return lerp(output_a, output_b, inverse_lerp(input_a, input_b, val));
    }

    private static float lerp(float a, float b, float val) {
        if (val < a)
        {
            return 0;
        }
        else if (val > b) {
            return 1;
        }
        return a + val * (b - a);

    }
    private static float inverse_lerp(float a, float b, float val) {
        float ret = (val - a) / (b - a);
        if (ret > b)
        {
            return b;
        }
        else if (ret < a) {
            return a;
        }
        return ret;
    }



}
