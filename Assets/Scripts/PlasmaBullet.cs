using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaBullet : MonoBehaviour{

    private float initTime;
    private const float MaxLifeTime = 5.0f;

    void OnCollisionEnter(Collision other) {
        Target target = other.gameObject.GetComponent<Target>();

        if(target != null) {
            target.Hit();
            Destroy(gameObject);
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody>().useGravity = false;
        this.initTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time-initTime > MaxLifeTime){
            Destroy(gameObject);
        }
    }
}
