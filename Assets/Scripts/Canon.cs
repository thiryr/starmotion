using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour{

    private bool shooting = false;

    private float muzzleOffset;

    public GameObject bullet;

    private const float FireRate = 2.0f;
    private const float RoundSpeed = 10.0f;


    private const float maxUpDownAngle = 10.0f;
    private const float maxLeftRightAngle = maxUpDownAngle;
    private float currentUpAngle = 0.0f;
    private float currentRightAngle = 0.0f;

    private Vector3 parentRotationPoint;



    private float nextShotTime = 0;

    void Start() {
        this.muzzleOffset = GetComponent<Renderer>().bounds.extents.z;
        this.parentRotationPoint = getInitialRotationPoint();
    }

    void Update() {
        if(this.shooting && Time.time > this.nextShotTime) {
            Shoot(Time.time);
        }
    }

    public void startShooting(){
        this.shooting = true;
    }
    public void stopShooting(){
        this.shooting = false;
    }

    private void Shoot(float time) {


                GameObject spawnedRound = Instantiate(
                    bullet,
                    transform.position + transform.forward * muzzleOffset,
                    transform.rotation
                );

                Rigidbody rb = spawnedRound.GetComponent<Rigidbody>();
                rb.velocity = spawnedRound.transform.forward * RoundSpeed;


                this.nextShotTime = time + (1 / FireRate);
    }

    private void uncheckedRotateUpDown(float angle){
        gameObject.transform.RotateAround(getRotationPoint(), Vector3.left, angle);
        currentUpAngle += angle;
    }
    private void uncheckedRotateLeftRight(float angle){
        gameObject.transform.RotateAround(getRotationPoint(), Vector3.up, angle);
        currentRightAngle += angle;
    }

    public void setUpDownRotation(float angle){
        if(angle>maxUpDownAngle){
            angle = maxUpDownAngle;
        }else if(angle<-maxUpDownAngle){
            angle = -maxUpDownAngle;
        }
        //reset to 0 rotation
        uncheckedRotateUpDown(-currentUpAngle);
        //set angle
        uncheckedRotateUpDown(angle);
    }
    public void setLeftRightRotation(float angle){
        if(angle>maxLeftRightAngle){
            angle = maxLeftRightAngle;
        }else if(angle<-maxLeftRightAngle){
            angle = -maxLeftRightAngle;
        }
        //reset to 0 rotation
        uncheckedRotateLeftRight(-currentRightAngle);
        //set angle
        uncheckedRotateLeftRight(angle);
    }

    //only used to get the fixed reference point in parent coordinates
    private Vector3 getInitialRotationPoint(){
        return gameObject.transform.localPosition - (gameObject.transform.forward * (gameObject.GetComponent<Renderer>().bounds.size.z/2));
    }
    // get the current world position of the rotation point
    private Vector3 getRotationPoint(){
        return gameObject.transform.parent.TransformPoint(this.parentRotationPoint);
    }
}
